#!/bin/sh

#################################################
#                                               #
# This script is coded for run in *nix-like env #
#                                               #
#################################################

### get local env var
IP=$(ip addr | grep "inet " | grep -v "127.0.0.1" | awk '{print $2}' | tr '\n' ' ' | sed 's/.$//')
IFS=' ' read -r -a IP_ARRAY <<< "$IP"
NIC=$(ip addr | grep "inet " | grep -v "127.0.0.1" | awk '{print $NF}' | tr '\n' ' ' | sed 's/.$//')
IFS=' ' read -r -a NIC_ARRAY <<< "$NIC"
HOSTNAME=$(hostname)
USERNAME=$(whoami)
HOMES=$(ls -l /home | grep -v total | awk '{print $9}' | tr '\n' ' ' | sed 's/.$//')
IFS=' ' read -r -a HOMES_ARRAY <<< "$HOMES"

### print test output
echo ""
echo "###########################################"
echo "#"
echo "# The IP address is: $IP (${#IP_ARRAY[@]})"
echo "# The NIC name is: $NIC (${#NIC_ARRAY[@]})"
echo "# The HOSTNAME is: $HOSTNAME"
echo "# The USERNAME is: $USERNAME"
echo "# The HOME(s) is/are: $HOMES (${#HOMES_ARRAY[@]})"
echo "#"
echo "###########################################"
echo "" 